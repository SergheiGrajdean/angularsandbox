﻿'use strict';

var phonecatApp = angular.module('phonecatApp', ['ui.bootstrap', 'ui.tree']);

phonecatApp.controller('PhoneListController', PhoneListController);

function PhoneListController($scope) {
    $scope.phones = [
        {
            'name': 'Nexus S',
            'snippet': 'Fast just got faster with Nexus S.'
        },
        {
            'name': 'Motorola XOOM™ with Wi-Fi',
            'snippet': 'The Next, Next Generation tablet.'
        },
        {
            'name': 'MOTOROLA XOOM™',
            'snippet': 'The Next, Next Generation tablet.'
        }
    ];

    $scope.init = function () {
        var originalName = $scope.newChannel.name;
        $scope.newChannel.name = 'a.b.c';
        $scope.addNewChannel();
        $scope.newChannel.name = 'a.c';
        $scope.addNewChannel();
        $scope.newChannel.name = originalName;
    };

    $scope.channel = 'aa.bb.cc.dd';
    $scope.body = 'test message';
    $scope.matchingChannel = 'test';

    $scope.newChannel = {
        name: 'a.b.c',
        description: ''
    };

    $scope.channels = buildRootNode();
    //$scope.channels = mock();
    //$scope.channels = mock2();


    $scope.messages = [];

    $scope.channelsDic = {
        'Admin': new Channel('Admin', [], [])
    };

    $scope.addNewChannel = function () {
        $scope.channelsDic[$scope.newChannel.name] = new Channel($scope.newChannel.name, []);
        $scope.newChannel.name = '';
        $scope.newChannel.description = '';
        $scope.channels = rebuildHierarchy($scope.channelsDic);
    };

    $scope.send = function () {

        $scope.matchingChannel = getMatchingChannel($scope.channel, $scope.channelsDic);
        console.log('Matching Channel : ');
        console.log($scope.matchingChannel);

        sendMessage({ channel: $scope.channel, body: $scope.body }, $scope.messages);

    };

    $scope.remove = function (scope) {
        scope.remove();
    };

    $scope.toggle = function (scope) {
        scope.toggle();
    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    function sendMessage(message, messages) {
        messages.push(message)
    };

    function getMatchingChannelFromTree(channel, channelHierarchy) {
        var splittedChannels = channel.split('.');
        var rootChannel = channelHierarchy[0];
        var rootChannelNames = channelHierarchy.channels;

        return getMatchingCannelRecursive(splittedChannels, 0, rootChannel.channels, rootChannel);

    };

    function getMatchingCannelRecursive(channelParts, index, channelNode) {
        /*
        if (itemsToAdd.length == index)
            return defaultChannel;
            */
        var currentPart = channelParts[index];
        var matchingNode = channelNode.channels.filter(function (node) { return node.title == currentPart; });

        if (matchingNode.length == 0) {
            return channelNode;
        };

        if (matchingNode.length == 1) {
            return getMatchingCannelRecursive(channelParts, index + 1, matchingNode.channels, matchingNode);
        };

        throw {
            message: 'Hierarchy contains more than one matching elemnt'
        };
    };

    function getMatchingChannel(channel, channelsDic) {
        var splittedChannels = channel.split('.');

        var targetChannel = channel;

        do {
            if (targetChannel in channelsDic)
                return channelsDic[targetChannel];

            var indexOfLastChannel = targetChannel.lastIndexOf('.');
            targetChannel = targetChannel.substring(0, indexOfLastChannel - 1);

        } while (targetChannel.indexOf('.') != -1);

        if (targetChannel in channelsDic)
            return channelsDic[targetChannel];

        return channelsDic['Admin'];
    }

    function rebuildHierarchy(channelsDic) {
        console.log('Rebuild hyerarchy');
        var newHierarchy = buildRootNode();
        for (var name in channelsDic) {
            if (channelsDic.hasOwnProperty(name) && name != 'Admin') {
                addChannelToHierarchy(name, newHierarchy[0].channels);
            }
        }
        console.log('New Hierarchy : ');
        console.log(newHierarchy);
        console.log(JSON.stringify(newHierarchy));
        return newHierarchy;
    };

    function addChannelToHierarchy(channelName, channelHierarchy) {
        var splittedChannelName = channelName.split('.');
        channelParsingRecursion(splittedChannelName, 0, channelHierarchy);
    };

    function channelParsingRecursion(itemsToAdd, index, hierarchy) {
        if (itemsToAdd.length == index)
            return [];

        var item = itemsToAdd[index];
        var matchingNode = hierarchy.filter(function (node) { return node.title == item; });
        if (matchingNode.length == 0) {
            var newChildNodes = channelParsingRecursion(itemsToAdd, index + 1, []);
            var newNode = new ChannelNode(item, item, newChildNodes);
            hierarchy.push(newNode);
            return hierarchy;
        };

        if (matchingNode.length == 1) {
            channelParsingRecursion(itemsToAdd, index + 1, matchingNode[0].channels);
            return hierarchy;
        };

        throw {
            message: 'Hierarchy contains more than one matching elemnt'
        };

    };

    function buildRootNode() {
        return [new ChannelNode('Admin', 'Admin', [])];
    };

    function Channel(name, messages) {
        this.name = name;
        this.messages = messages;
    }

    function ChannelNode(id, title, channels, messages) {
        this.id = id;
        this.title = title;
        this.channels = channels;
        this.messages = messages;
    }

    function mock() {
        return [
            {
                "id": "Admin",
                "title": "Admin",
                "channels": [{
                    "id": "a",
                    "title": "a",
                    "channels": [{
                        "id": "b",
                        "title": "b",
                        "channels": [{
                            "id": "c",
                            "title": "c",
                            "channels": []
                        }]
                    },
                    {
                        "id": "x",
                        "title": "x",
                        "channels": []
                    }]
                }, {
                    "id": "b",
                    "title": "b",
                    "channels": [{ "id": "v", "title": "v", "channels": [] }]
                }]
            }]
    };

    function mock2() {
        return [{
            'id': 1,
            'title': 'node1',
            'channels': [
              {
                  'id': 11,
                  'title': 'node1.1',
                  'channels': [
                    {
                        'id': 111,
                        'title': 'node1.1.1',
                        'channels': []
                    }
                  ]
              },
              {
                  'id': 12,
                  'title': 'node1.2',
                  'channels': []
              }
            ]
        }, {
            'id': 2,
            'title': 'node2',
            'nodrop': true, // An arbitrary property to check in custom template for nodrop-enabled
            'channels': [
              {
                  'id': 21,
                  'title': 'node2.1',
                  'channels': []
              },
              {
                  'id': 22,
                  'title': 'node2.2',
                  'channels': []
              }
            ]
        }, {
            'id': 3,
            'title': 'node3',
            'channels': [
              {
                  'id': 31,
                  'title': 'node3.1',
                  'channels': []
              }
            ]
        }];
    };

    $scope.init();
}

